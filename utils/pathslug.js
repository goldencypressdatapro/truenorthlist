const slugify = require('slugify');


slugify.extend({'&': 'AnD'})

var _opts=
{
  lower: true,      // convert to lower case, defaults to `false`
  strict: true,     // strip special characters except replacement, defaults to `false`
  locale: 'ru'       // language code of the locale to use
};

module.exports = function(content) 
{

   var _rz = content.replace(/[^\/]+/gi, t=>slugify(t,_opts) );

   return _rz;

}




