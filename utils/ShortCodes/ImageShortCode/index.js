const Image = require("@11ty/eleventy-img");
const sharp = require("sharp");
const path = require("path");

module.exports =   async  function imageShortcode(src, alt) 
{
    if (!alt) {
        throw new Error(`Missing \`alt\` on myImage from: ${src}`);
      }
  
      let stats =  await   Image(src, {
        widths: [ 225 , 320, 640, 960, 1200, 1800, 2400],
        formats: ["avif" , "webp", "jpeg"],
        urlPath: "/images/slider",
        outputDir: "./_site/images/slider",

        filenameFormat: function (id, src, width, format, options) {
          const extension = path.extname(src);
          const name = path.basename(src, extension);
      
          return `${name}-${width}w.${format}`;
        }



      });
  
      let lowestSrc = stats["jpeg"][0];
  
      const placeholder = await sharp(lowestSrc.outputPath)
        .resize({ fit: sharp.fit.inside })
        .jpeg({ quality:30  })
        .blur(2)
        .toBuffer();
  
      const base64Placeholder = `data:image/png;base64,${placeholder.toString(
        "base64"
      )}`;
  
      const srcset = Object.keys(stats).reduce(
        (acc, format) => ({
          ...acc,
          [format]: stats[format].reduce(
            (_acc, curr) => `${_acc} ${curr.srcset} ,`,
            ""
          ),
        }),
        {}
      );
  
//      const source = `<source type="image/webp" data-srcset="${srcset["webp"]}" >`;
      const source = `<source type="image/webp" data-srcset="${srcset["avif"]}" >`;
  
      const img = `<img
        class="lazy w-full h-full object-cover"
        alt="${alt}"
        src="${base64Placeholder}"
        data-src="${lowestSrc.url}"
        data-sizes='(min-width: 1024px) 1024px, 100vw'
        data-srcset="${srcset["jpeg"]}"
        width="${lowestSrc.width}"
        height="${lowestSrc.height}">`;
  

        var  sizes="(min-width: 1024px) 1024px, 100vw";


/*
        return `
          <img
            class="w-full h-full object-cover"
            src="${base64Placeholder}"
            alt="${alt}">
        `;
*/  



        return `<picture>
        ${Object.values(stats).map(imageFormat => {
          return `  <source type="${imageFormat[0].sourceType}" srcset="${imageFormat.map(entry => entry.srcset).join(", ")}" sizes="${sizes}">`;
        }).join("\n")}
          <img
            class="w-full h-full object-cover"
            src="${base64Placeholder}"
            loading="lazy"
            alt="${alt}">
        </picture>`;



        return `<picture>
        ${Object.values(stats).map(imageFormat => {
          return `  <source type="${imageFormat[0].sourceType}" srcset="${imageFormat.filter((_,j)=>j>0).map(entry => entry.srcset).join(", ")}" sizes="${sizes}">`;
        }).join("\n")}
          <img
            class="w-full h-full object-cover"
            src="${base64Placeholder}"
            width="${lowestSrc.width}"
            height="${lowestSrc.height}"
            alt="${alt}"
            loading="lazy"
            decoding="async">
        </picture>`;
  






    //    return `<div class="image-wrapper"><picture> ${source} ${img} </picture></div>`;




        return `<picture> ${source} ${img} </picture>`;
    }
