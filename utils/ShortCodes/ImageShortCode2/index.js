const Image = require("@11ty/eleventy-img");
const sharp = require("sharp");
const path = require("path");

module.exports =   async  function imageShortCode2(src, alt) 
{

    if (!alt) {   throw new Error(`Missing \`alt\` on myImage from: ${src}`);    }


    let stats =  await   Image(src, {
        widths: [ 225 , 320, 640, 960, 1200, 1800, 2400],
        formats: ["jpeg"],
        urlPath: "/images/slider",
        outputDir: "./_site/images/slider",

        filenameFormat: function (id, src, width, format, options) {
          const extension = path.extname(src);
          const name = path.basename(src, extension);
          return `${name}-${width}w.${format}`;
        }
      });
  
      let _stats_jpeg=stats["jpeg"];
      let lowestSrc = _stats_jpeg[0];
//      let highestSrc = stats_jpeg[_stats_jpeg.length-1];
  
      const placeholder = await sharp(lowestSrc.outputPath)
//        .resize({ fit: sharp.fit.inside })
        .jpeg({ quality:30  })
//        .blur(2)
        .toBuffer();
  
      const base64Placeholder = `data:image/png;base64,${placeholder.toString(
        "base64"
      )}`;

      const srcset = Object.keys(stats).reduce(
        (acc, format) => ({
          ...acc,
          [format]: stats[format].reduce(
            (_acc, curr) => `${_acc} ${curr.srcset} ,`,
            ""
          ),
        }),
        {}
      );


      const source = `<source type="image/webp" data-srcset="${srcset["jpeg"]}" >`;

      let  _json=JSON.stringify(lowestSrc);

        const _img_markup=`<img data-src="${lowestSrc.url}" class="swiper-lazy  w-full h-full object-cover "  alt="${alt}"  data-srcset="${srcset["jpeg"]}"  src="${base64Placeholder}" >`;


        return  _img_markup;


}