function _test_0(_node,_path)
{
    if (_node.url==_path)   return  true;
    return  _node.children.some(_n => _test_0(_n,_path));
}

function _inpath(_node,_path)
{
    if (_node.children.length==0)   return  false;
    return  _node.children.some(_n => _test_0(_n,_path));
}

function _iscurrent(_node,_path)
{
    if (_node.url==_path)   return  true;
    return  false;
}


module.exports={inpath:_inpath,iscurrent:_iscurrent}