
| Public Place Category                 | &emsp;&emsp;Qty     |
|--------------------------------------:|--------:|
| Airports                              | 20,012  |
| Colleges                              | 11,516  |
| Convention Centers                    | 374     |
| Corrections                           | 1,685   |
| Courts                                | 15,876  |
| Disaster Response                     | 3,246   |
| Driver's Licenses DMV                 | 3,892   |
| Drug Treatment Centers                | 11,481  |
| Electric Utilities                    | 240     |
| Employment / Unemployment             | 3,011   |
| Fire/Rescue/Hazmat                    | 30,532  |
| Fish and Wildlife Services            | 1,275   |
| Hospitals                             | 5,465   |
| IRS Offices                           | 465     |
| Libraries                             | 19,087  |
| Museums                               | 1,142   |
| National Parks                        | 636     |
| Nursing Homes                         | 15,761  |
| OSHA                                  | 170     |
| Passport Application Facility         | 7,910   |
| Police                                | 11,821  |
| Police Municipal                      | 10,876  |
| Ports \(U\.S\.\)                      | 283     |
| Postal Service                        | 35,162  |
| Private Schools                       | 30,742  |
| Public Assistance \- Social Services  | 3,637   |
| Public Defender                       | 1,373   |
| Public Health                         | 3,146   |
| Public Schools                        | 102,392 |
| School Districts                      | 16,470  |
| Social Security                       | 1,154   |
| State Parks                           | 3,687   |
| Transportation                        | 594     |
| USDA Service Centers                  | 2,556   |
| Vital Records Offices                 | 55      |
| Zoos And Aquariums                    | 227     |
| Total                                 | 374,519 |
