

| Function / Description        | &emsp;&emsp;Category                 |
|-------------------------------|--------------------------------------|
| COMMERCE                      | airports                             |
| TRANSPORTATION                | airports                             |
| RECREATION                    | Aquarium                             |
| OCEANOGRAPHY                  | Aquarium                             |
| CULTURAL                      | Aquarium                             |
| CULTURAL                      | Botanical Gardens                    |
| AGRICULTURE                   | Botanical Gardens                    |
| ENVIRONMENT                   | Botanical Gardens                    |
| BIOLOGICAL SCIENCE            | Botanical Gardens                    |
| EDUCATION                     | Colleges                             |
| COMMERCE                      | Convention Centers                   |
| JUSTICE                       | Corrections                          |
| GOVERNMENT                    | Courts                               |
| NON\-ELECTED OFFICIALS        | Courts                               |
| JUSTICE                       | Courts                               |
| LEGAL AFFAIRS                 | Courts                               |
| ELECTED OFFICIALS             | Courts                               |
| TRANSPORTATION                | Driver's Licenses                    |
| GENERAL & ADMINISTRATION      | Driver's Licenses                    |
| GOVERNMENT                    | Driver's Licenses                    |
| HEALTH                        | Drug Treatment Centers               |
| HUMAN SERVICE                 | Drug Treatment Centers               |
| ENERGY                        | Electric Companies                   |
| PUBLIC WORKS                  | Electric Companies                   |
| TRAINING                      | Employment / Unemployment            |
| PERSONNEL                     | Employment / Unemployment            |
| LABOR                         | Employment / Unemployment            |
| CIVIL RIGHTS                  | Employment / Unemployment            |
| SAFETY                        | Fire & Rescue                        |
| DISASTER                      | Fire & Rescue                        |
| PUBLIC WORKS                  | Fire & Rescue                        |
| SAFETY                        | Fire/Rescue/Hazmat                   |
| PUBLIC WORKS                  | Fire/Rescue/Hazmat                   |
| DISASTER                      | Fire/Rescue/Hazmat                   |
| OCEANOGRAPHY                  | Fish and Wildlife Services           |
| ENVIRONMENT                   | Fish and Wildlife Services           |
| NATURAL RESOURCES             | Fish and Wildlife Services           |
| BIOLOGICAL SCIENCE            | Fish and Wildlife Services           |
| RECREATION                    | Golf Courses \- Public               |
| HEALTH                        | Hospitals                            |
| SCIENTIFIC & MEDICAL RESEARCH | Hospitals                            |
| GENERAL & ADMINISTRATION      | IRS Offices                          |
| ADMINISTRATIVE                | IRS Offices                          |
| GOVERNMENT                    | IRS Offices                          |
| INFORMATION TECHNOLOGY        | Libraries                            |
| CULTURAL                      | Libraries                            |
| GOVERNMENT                    | Libraries \- Presidential            |
| CULTURAL                      | Museums                              |
| CULTURAL                      | Museums \- Children's                |
| RECREATION                    | National Parks                       |
| NATURAL RESOURCES             | National Parks                       |
| HUMAN SERVICE                 | Nursing Homes                        |
| HOUSING                       | Nursing Homes                        |
| SAFETY                        | OSHA                                 |
| LABOR                         | OSHA                                 |
| HEALTH                        | OSHA                                 |
| GENERAL & ADMINISTRATION      | Passport Application Facility        |
| ADMINISTRATIVE                | Passport Application Facility        |
| PHYSICAL SCIENCE              | Planetariums                         |
| SPACE                         | Planetariums                         |
| SCIENCE&TECHNOLOGY            | Planetariums                         |
| DISASTER                      | Police                               |
| SAFETY                        | Police                               |
| ENFORCEMENT                   | Police                               |
| GOVERNMENT                    | Political Action Committees          |
| PUBLIC AFFAIRS                | Political Action Committees          |
| TRANSPORTATION                | Ports \(U\.S\.\)                     |
| PUBLIC WORKS                  | Postal Service                       |
| EDUCATION                     | Private Schools                      |
| HUMAN SERVICE                 | Public Assistance \- Social Services |
| SOCIAL SCIENCE                | Public Assistance \- Social Services |
| HUMAN SERVICE                 | Public Health                        |
| HEALTH                        | Public Health                        |
| EDUCATION                     | Public Schools                       |
| PUBLIC WORKS                  | Rural Electric Cooperatives          |
| EDUCATION                     | School Districts                     |
| SOCIAL SCIENCE                | Social Security                      |
| HUMAN SERVICE                 | Social Security                      |
| RECREATION                    | State Parks                          |
| NATURAL RESOURCES             | State Parks                          |
| TRANSPORTATION                | Transportation                       |
| AGRICULTURE                   | USDA Service Centers                 |
| PUBLIC WORKS                  | Utility Services                     |
| ADMINISTRATIVE                | Vital Records Offices                |
| GENERAL & ADMINISTRATION      | Vital Records Offices                |
| GOVERNMENT                    | Vital Records Offices                |
| ENVIRONMENT                   | Zoos                                 |
