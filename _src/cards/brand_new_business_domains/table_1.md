

| Foreign / International   ||
|---------------------|------------:|
| Australia           |  250,000    |
| Austria	          |  200,000    |
| Canada	          |  400,000    |
| France	          |  550,000    |
| Germany	          |  1,300,000  |
| Italy	              |  450,000    |
| Russian Federation  |	 500,000    |
| Spain	              |  450,000    |
| United Kingdom	  |  1,350,000  |

