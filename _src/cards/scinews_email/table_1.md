

| Research Areas Selects    | &emsp;&emsp;US     | &emsp;&emsp;Canada  | &emsp;&emsp;International |
|---------------------------|-------:|--------:|--------------:|
| Biomarker                 | 4,323  | 2,054   | 1,434         |
| Cell Biology              | 6,543  | 1,982   | 2,876         |
| Combinatorial Chemistry   | 3,543  | 873     | 1,083         |
| Drug Discovery            | 6,854  | 1,289   | 3,765         |
| Genomics                  | 12,323 | 3,872   | 5,423         |
| Immunology                | 13,242 | 2,898   | 3,211         |
| Microbiology              | 3,865  | 1,872   | 3,552         |
| Medicinal Chemistry       | 3,765  | 1,027   | 1,089         |
| Neuroscience              | 8,543  | 2,010   | 2,676         |
| Oncology                  | 6,134  | 1,652   | 4,098         |
| Proteomics                | 13,023 | 3,762   | 6,534         |
| Stem Cell Research        | 4,504  | 1,552   | 2,702         |