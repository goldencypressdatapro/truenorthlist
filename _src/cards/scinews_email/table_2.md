

| Research Techniques/ Technologies Selects      | &emsp;&emsp;US     | &emsp;&emsp;Canada  | &emsp;&emsp;International |
|------------------------------------------------|-------:|--------:|--------------:|
| Electrophoresis/Western blots                  | 13,019 | 2,982   | 3,552         |
| ELISA/ELISPOT                                  | 7,622  | 2,025   | 2,478         |
| Gene/Protein Expression                        | 8,352  | 2,225   | 2,971         |
| Imaging/Microscopy                             | 2,363  | 1,011   | 1,625         |
| LC/GC/Mass Spectrometry                        | 4,633  | 972     | 2,077         |
| Microarrays                                    | 4,032  | 1,237   | 2,238         |
| Nucleic acid Sequencing                        | 7,823  | 1,432   | 2,672         |
| PCR/RT\-PCR/Q\-PCR                             | 14,389 | 3,662   | 4,232         |
| Plasmid/DNA Purification                       | 3,877  | 1,321   | 2,672         |
| RNA analysis                                   | 8,298  | 2,411   | 3,673         |
| SiRNA/RNAi/MicroRNA                            | 6,721  | 1,892   | 2,782         |
