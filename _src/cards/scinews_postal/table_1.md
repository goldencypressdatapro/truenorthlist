

| Research Areas Selects    |     &emsp;&emsp;US | &emsp;&emsp;Canada  | &emsp;&emsp;International |
|---------------------------|-------:|--------:|--------------:|
| Biomarker                 | 1,965  | 654     | 1,323         |
| Cell Biology              | 8,543  | 1,435   | 2,017         |
| Combinatorial Chemistry   | 1,675  | 622     | 1,522         |
| Drug Discovery            | 3,243  | 1,654   | 1,982         |
| Genomics                  | 9,321  | 3,765   | 2,876         |
| Immunology                | 12,934 | 4,434   | 2,543         |
| Microbiology              | 7,652  | 3,223   | 1,762         |
| Medicinal Chemistry       | 3,432  | 761     | 1,592         |
| Neuroscience              | 6,543  | 987     | 1,582         |
| Oncology                  | 8,971  | 1,045   | 1,772         |
| Proteomics                | 13,987 | 4,776   | 4,817         |
| Stem Cell Research        | 9,543  | 1,564   | 2,023         |

