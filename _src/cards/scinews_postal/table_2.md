

| Research Techniques/ Technologies Selects      | &emsp;&emsp;US     | &emsp;&emsp;Canada  | &emsp;&emsp;International |
|------------------------------------------------|-------:|--------:|--------------:|
| Electrophoresis/Western blots                  | 5,271  | 1,292   | 2,982         |
| ELISA/ELISPOT                                  | 8,726  | 1,028   | 2,654         |
| Gene/Protein Expression                        | 9,352  | 2,787   | 2,012         |
| Imaging/Microscopy                             | 1,872  | 876     | 545           |
| LC/GC/Mass Spectrometry                        | 3,987  | 894     | 987           |
| Microarrays                                    | 2,876  | 1,126   | 1,043         |
| Nucleic acid Sequencing                        | 9,762  | 1,766   | 1,326         |
| PCR/RT\-PCR/Q\-PCR                             | 8,932  | 2,376   | 2,776         |
| Plasmid/DNA Purification                       | 3,873  | 1,125   | 865           |
| RNA analysis                                   | 7,635  | 2,522   | 976           |
| SiRNA/RNAi/MicroRNA                            | 6,221  | 1,928   | 1,587         |
