
-   Subscriptions to two industry-leading journals; ASPEN's journals, the Journal of 
    Parenteral and Enteral Nutrition (JPEN) and Nutrition in Clinical Practice (NCP)
-   Discounts on cutting-edge professional development and research programs