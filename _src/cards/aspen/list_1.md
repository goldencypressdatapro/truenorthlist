

1.  The association's annual meeting, the ASPEN Nutrition Science & Practice Conference, 
    is the only scientific and clinical conference dedicated to specialized nutrition 
    support research and clinical practice.
1.  ASPEN also publishes a variety of resources to drive safe, efficacious patient care, 
    including Guidelines, Standards, Safe Practices, publications and continuing education programs.
1.  Through the ASPEN Rhoads Research Foundation, ASPEN supports innovation in advancing the 
    science of nutrition support.
1.  The Society also works closely with other health care organizations to advance a patient-centered 
    approach to nutrition care and with government agencies about the optimal use of nutrition therapies.