module.exports = {
    getPath4CSS: (assetName) => {
        if (false && process.env.NODE_ENV === "production") {
            const assets = require("../_includes/manifest_css.json");
            const modulePath = assets[assetName];
            if(!modulePath) {
              throw new Error(`error with getAsset, ${assetName} does not exist in manifest_css.json`);
            }
            return `${modulePath}`;
        } else {
            return `${assetName}`;
        }
    }
}