const   pathslug  = require( './utils/pathslug.js')
const   path = require('path');
const   yaml = require("js-yaml");
const   markdownit = require('markdown-it');

const { EleventyRenderPlugin } = require("@11ty/eleventy");


const eleventyNavigationPlugin = require("@11ty/eleventy-navigation");

//const { compress } = require('eleventy-plugin-compress');

const svgContents = require("eleventy-plugin-svg-contents");





const cacheBuster = require('@mightyplow/eleventy-plugin-cache-buster');
//const AssetPath=require("./Utilities/11ty/filters/AssetPathFilter.js")

//const fs = require('fs');
//const path = require('path');

const { NODE_ENV = 'production' } = process.env

const isProduction = NODE_ENV === 'production'


function is4htmlmin(_path)
{
  return  false;

if (process.env.NODE_ENV != "production") return  false;
if (!/html$/.test(_path)) return  false;
if (/cards-by-tree/.test(_path)) return  false;

  return  true;
}



module.exports= function(eleventyConfig)
{


    eleventyConfig.addTransform("htmlmin", function(content, outputPath) {
        //    return  content;
        //  console.log('????? >>> ',JSON.stringify(outputPath));
          if( is4htmlmin(outputPath) ) 
          {
            let htmlmin = require("html-minifier");
            let minified = htmlmin.minify(content, {
              useShortDoctype: true,
              removeComments: true,
              collapseWhitespace: true
            });
            return minified;
          }
        
          return content;
        });
        
        





    if (isProduction)
    {
      const cacheBusterOptions = {};
      eleventyConfig.addPlugin(cacheBuster(cacheBusterOptions));
    }




//    eleventyConfig.addFilter('assetPath', AssetPath(__dirname));

eleventyConfig.addCollection("articles", collectionApi => collectionApi.getFilteredByGlob("_src/articles/**/index.*") );
eleventyConfig.addCollection("cards", collectionApi => collectionApi.getFilteredByGlob("_src/cards/**/index.*") );


eleventyConfig.addPlugin(eleventyNavigationPlugin);
eleventyConfig.addPlugin(EleventyRenderPlugin);

eleventyConfig.addFilter('inpath',require( './utils/inpath.js').inpath)
eleventyConfig.addFilter('iscurrent',require( './utils/inpath.js').iscurrent)


eleventyConfig.addPlugin(svgContents);


eleventyConfig.addFilter('markdown', function(value) {
    let markdown = markdownit({   html: true });
    return markdown.render(value);
  });
  
  

eleventyConfig.addFilter('bhtml', function(value) 
{
  var b_html = require('js-beautify');
  var _out=b_html.html_beautify(value,{ indent_empty_lines: false, preserve_newlines: false });
  return _out;
});




eleventyConfig.addPairedShortcode('datacardbody', (content) => {
    return `<p class="dataCardBody">${content}</p>`;
  });

  eleventyConfig.addPairedShortcode('bodyHeaderRed', (content) =>  `<p class="bodyHeaderRed">${content}</p>` );
  eleventyConfig.addPairedShortcode('bodyCopy', (content) =>  `<p class="bodyCopy">${content}</p>` );




  eleventyConfig.addWatchTarget( isProduction ? "./_styles/out/**":"./_styles/in/**");

eleventyConfig.addWatchTarget( "./_src/**");
eleventyConfig.addWatchTarget( "./_src/cards/**");
eleventyConfig.addWatchTarget( "./_src/articles/**");




//  eleventyConfig.addPassthroughCopy("_src/**/images/**");

eleventyConfig.addPassthroughCopy("_src/cards/**/images/**");
eleventyConfig.addPassthroughCopy("_src/articles/**/images/**");
eleventyConfig.addPassthroughCopy("_src/images/**");

eleventyConfig.addPassthroughCopy("_src/downloads/**");


eleventyConfig.addPassthroughCopy({  "_styles/out/tailwind.css":"css/tailwind.css" });
  

eleventyConfig.addFilter("jsonify", _arg=> JSON.stringify(_arg)  );

//  eleventyConfig.addPassthroughCopy("_src/images/**");
//eleventyConfig.addPassthroughCopy({  "_src/styles/out/tailwind.css":"css/tailwind.css" });
//eleventyConfig.addPassthroughCopy({  "_src/styles/out/tailwind.css":"/css/" });







//  eleventyConfig.addDataExtension("yaml", contents => yaml.safeLoad(contents));
  eleventyConfig.addDataExtension("yaml", contents => yaml.load(contents));


  eleventyConfig.addLayoutAlias('default', 'layouts/base.njk');

  eleventyConfig.addLayoutAlias('basecard', 'layouts/basepage4card.njk');

  eleventyConfig.addLayoutAlias('basepage', 'layouts/basepage.njk');
  
  //  eleventyConfig.addLayoutAlias('baseart', 'layouts/art_layout2.njk');
  
  eleventyConfig.addLayoutAlias('baseart', 'layouts/basepage4article.njk');
  eleventyConfig.addLayoutAlias('basehome', 'layouts/basepage4home.njk');
  
  eleventyConfig.addFilter('pathslug', obj => { return pathslug(obj);  });


  eleventyConfig.addNunjucksAsyncShortcode("Image", require('./utils/ShortCodes/ImageShortCode'));

  eleventyConfig.addNunjucksAsyncShortcode("Image2", require('./utils/ShortCodes/ImageShortCode2'));

  
//  eleventyConfig.addPlugin(compress, {   /* algorithm : 'gzip'  */  /* Optional options. */  });



eleventyConfig.setBrowserSyncConfig({
  callbacks: {
    ready: function (err, browserSync) {
        browserSync.addMiddleware("*", function (req, res) {
        res.writeHead(302, {
            location: "/404.html"
        });
        res.end("Redirecting!");
    });
    }
  }
});


eleventyConfig.setWatchThrottleWaitTime(500); // in milliseconds




return   {

    dir:{
        input: "_src",
        includes: "_includes",
        output: "_site"
        },

        templateFormats: ["njk", "md", "ejs","11ty.js"],


        htmlTemplateEngine: "njk",
        markdownTemplateEngine: "njk"
    



}



}

